import Layout from '../components/layout/layout';
import '../styles/globals.scss';
import 'swiper/swiper-bundle.min.css';

function MyApp({ Component, pageProps }) {
	return (
		<Layout>
			<Component {...pageProps} />
		</Layout>
	);
}

export default MyApp;
