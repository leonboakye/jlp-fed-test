import Link from 'next/link';
import axios from 'axios';
import ProductCarousel from '../../components/product-carousel/product-carousel';

import styles from './productDetail.module.scss';
export async function getServerSideProps(context) {
	const id = context.params.id;
	const { data } = await axios.get(
		'https://api.johnlewis.com/mobile-apps/api/v1/products/' + id
	);

	return {
		props: { data },
	};
}

const ProductDetail = ({ data }) => {
	const renderSpecificationList = () => {
		return (
			data &&
			data.details.features[0].attributes.map((item, i) => {
				return i <= 8 ? (
					<li className={styles.listItem} key={i}>
						<div>
							{item.name && <p>{item.name}</p>}
							{item.value && <p>{item.value}</p>}
						</div>
						<hr className={styles.hr} />
					</li>
				) : null;
			})
		);
	};

	return (
		<div className={styles.productDetail}>
			<div className={styles.titleWrap}>
				{data.title && <h1 className={styles.title}>{data.title}</h1>}
				<Link href='/'>
					<a>{'<'}</a>
				</Link>
			</div>
			<div className={styles.productDetailWrap}>
				<div className={styles.carouselWrap}>
					<ProductCarousel image={data.media.images.urls} />
				</div>
				<div className={styles.offerServicesWrap}>
					{data.price.now && (
						<div>
							<p className={styles.price}>£{data.price.now}</p>
						</div>
					)}
					<div>
						{data.displaySpecialOffer && <p>{data.displaySpecialOffer}</p>}
						{data.additionalServices.includedServices && (
							<p>{data.additionalServices.includedServices}</p>
						)}
					</div>
				</div>

				<div className={styles.productInfoWrap}>
					<h3>Product information</h3>
					<article className={styles.productInfoText}>
						<p>
							Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestias
							veritatis perspiciatis, ea quos tempore ipsum odit aspernatur qui, sequi
							quae et! Voluptatibus assumenda ab neque eveniet, blanditiis dolorum
							corrupti ipsa.
						</p>
					</article>
					{data.code && (
						<p className={styles.productCode}>{`Product code: ${data.code}`}</p>
					)}
					<hr className={styles.hr} />
				</div>
				<div className={styles.productSpecification}>
					<h3 className={styles.specification}>Product specification</h3>
					<hr className={styles.hr} />
				</div>
				<ul>{renderSpecificationList()}</ul>
			</div>
		</div>
	);
};

export default ProductDetail;
