import axios from 'axios';
import Head from 'next/head';
import Link from 'next/link';
import styles from './index.module.scss';
import ProductListItem from '../components/product-list-item/product-list-item';

export async function getServerSideProps() {
	const { data } = await axios.get(
		'https://api.johnlewis.com/search/api/rest/v2/catalog/products/search/keyword?q=dishwasher&key=AIzaSyDD_6O5gUgC4tRW5f9kxC0_76XRC8W7_mI'
	);

	return {
		props: {
			data,
		},
	};
}

const Home = ({ data }) => {
	const renderDishWasher = () => {
		const items = data && data.products;
		return items.map((item, i) => {
			return i + 1 <= 20 ? (
				<Link
					key={item.productId}
					href={{
						pathname: '/product-detail/[id]',
						query: { id: item.productId },
					}}
				>
					<a className={styles.link}>
						<div className={styles.content}>
							<ProductListItem
								image={item.image}
								price={item.variantPriceRange.display.max}
								title={item.title}
							/>
						</div>
					</a>
				</Link>
			) : null;
		});
	};
	return (
		<div>
			<Head>
				<title>JL &amp; Partners | Home</title>
				<meta name='keywords' content='shopping' />
			</Head>
			<section className={styles.main}>
				<h1 className={styles.pageTitle}>Dishwashers</h1>
				<div className={styles.container}>
					<div className={styles.gridContainer}>{renderDishWasher()}</div>
				</div>
			</section>
		</div>
	);
};

export default Home;
