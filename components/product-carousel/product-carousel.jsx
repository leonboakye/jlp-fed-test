import { useState } from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Navigation } from 'swiper';
// import required modules
import SwiperCore, { Pagination } from 'swiper/core';
import 'swiper/components/pagination/pagination.min.css';

import 'swiper/swiper.min.css';

import styles from './product-carousel.module.scss';

SwiperCore.use([Pagination]);

const ProductCarousel = ({ image }) => {
	const renderSlideImage = () => {
		return (
			image &&
			image.map((item) => {
				return (
					<SwiperSlide className={styles.swiperSlide}>
						<img src={item} alt='' />
					</SwiperSlide>
				);
			})
		);
	};
	return (
		<div className={styles.productCarousel} aria-pretend='true'>
			<Swiper
				className={styles.mySwiper}
				spaceBetween={10}
				loop={true}
				slidesPerView={1}
				pagination={{
					clickable: true,
				}}
			>
				{renderSlideImage()}
			</Swiper>
		</div>
	);
};

export default ProductCarousel;
