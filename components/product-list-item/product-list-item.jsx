import styles from './product-list-item.module.scss';

const ProductListItem = ({ image, price, title }) => {
	return (
		<div className={styles.content}>
			{image && (
				<div>
					<img src={image} alt='dishwashers' style={{ width: '100%' }} />
				</div>
			)}
			{title && <h2 className={styles.title}>{title}</h2>}
			{price && <p className={styles.price}>{price}</p>}
		</div>
	);
};

export default ProductListItem;
